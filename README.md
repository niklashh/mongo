# Mongo

> Link to the docker hub repo:
> https://hub.docker.com/r/niklashh/mongo/

This is a small project to improve the mongo (https://hub.docker.com/_/mongo/)
docker image to allow creating a custom user and a
custom database using environment variables.

A usage example can be found on the docker hub page:
https://hub.docker.com/r/niklashh/mongo/

Current build is MongoDB `v4.0.3`

## Plans for improvement

Currently no improvement plans.
**Any suggestions are welcome!**

I plan to update the image to the latest mongo
whenever I think it's *necessary*