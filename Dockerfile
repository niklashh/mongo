FROM mongo:latest

# Override mongo's docker-entrypoint
COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 27017

# The command to run in the docker-entrypoint
# Can be accessed in the entry script using "$@"
CMD ["mongod"]
